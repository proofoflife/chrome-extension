import { NgModule }         from '@angular/core';
import { BrowserModule }    from '@angular/platform-browser';
import { FormsModule }      from '@angular/forms';
import { HttpModule }       from '@angular/http';

import { AppRoutingModule } from './/app-routing.module';
import { AuthGuard }        from './auth-guard.service';
import { UserService }      from './user-service.service';
import { DataService }      from './data-service.service';
// import { UtilsService }     from '../services/utils.service';

import { AppComponent }     from './app.component';
import { LoginComponent }   from './login/login.component';
import { CheckInComponent } from './check-in/check-in.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CheckInComponent,
    // FormsModule,
    // HttpModule,
    // AppRoutingModule
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [
    AuthGuard,
    UserService,
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
