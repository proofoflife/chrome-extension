import { Component, OnInit } from '@angular/core';
import { NgForm }            from '@angular/forms';

import { UserService }       from '../user-service.service';
import { DataService }       from '../data-service.service';
import { AuthGuard }         from '../auth-guard.service';
import { User }              from '../models/user';
import { Router }            from '@angular/router';

@Component({
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {

  title = 'PROOF of LIFE';
  rememberMe: boolean;

  isRequesting : boolean = false;
  errors : string;
  
  userLoginModel: User = {
    email: '',
    pin: '',
  };

  constructor(private dataService: DataService, private userService: UserService, private router : Router) {}

  ngOnInit() {
    this.userLoginModel.email = sessionStorage.getItem('email');
    if(this.userLoginModel.email != null || localStorage.getItem('rememberMe') == "true") {
        this.rememberMe = true;
    }
  }

  login() {
    this.isRequesting = true;
    this.userService.isRequesting = true;

    let utcOffset = new Date().getTimezoneOffset();

    this.userService.login(this.userLoginModel.email, this.userLoginModel.pin, utcOffset, (userInfo) => {

        if (!userInfo) {
            this.errors = 'Email and/or pin is incorrect.';
            this.isRequesting = false;
            this.userService.isRequesting = false;
            return;
        }

        localStorage.setItem('rememberMe', String(this.rememberMe))
        sessionStorage.setItem('authentication_token', userInfo.auth_token);
        sessionStorage.setItem('next_PoL_action', userInfo.next_PoL_action);
        
      this.dataService.authGetData(`api/users/user?id=${userInfo.id}`, (currentUser) => {
        sessionStorage.setItem('current_user', JSON.stringify(currentUser));
            
            if (this.rememberMe) { sessionStorage.setItem('email', this.userLoginModel.email); }
                else { sessionStorage.removeItem('email'); }

            let status: number = currentUser.Status;
                sessionStorage.setItem('status', status.toString())

            this.router.navigate(['/check-in']);

            this.isRequesting = false;
            this.userService.isRequesting = false;
            
  }, (error) => {
    this.isRequesting = false;
    this.userService.isRequesting = false;
    this.errors = error;
    return;
  });
    }, (error) => {
        this.isRequesting = false;
        this.userService.isRequesting = false;
        this.errors = error;
    });
}

}
