import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { DataService } from './data-service.service'
import { Router } from '@angular/router';

@Injectable()
export class UserService {

  public isRequesting: boolean = false;

    constructor(private http: Http, private dataService: DataService, private router: Router) { }

    login(username: string, pin: string, utcOffset: number, fn: ((value) => void), fnError: ((error) => void) = null): void {
        let serviceRequestBody: { username: string, pin: string, utcOffset: number } = { username: username, pin: pin, utcOffset: utcOffset };
        this.dataService.postData('api/wa/login', serviceRequestBody, fn, fnError);
    }

    isLoggedIn(): boolean {
      return !!sessionStorage.getItem('authentication_token')
          && !!sessionStorage.getItem('identifier')
          && !!sessionStorage.getItem('current_user');
  }

    logout(): void {
      sessionStorage.removeItem('authentication_token');
      sessionStorage.removeItem('user_id');
      sessionStorage.removeItem('subsc');
      sessionStorage.removeItem('current_user');
      this.router.navigate(['/']);
  }

}
