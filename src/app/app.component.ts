import { Component }    from '@angular/core';
import { UserService }  from './user-service.service';
import { DataService }  from './data-service.service';
import { Router }       from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})

export class AppComponent {
  
  title = 'PROOF of LIFE';

  constructor( private dataService: DataService, private userService: UserService ) {}

  loggedIn() {
      return this.userService.isLoggedIn();
  }
  isRequesting() {
      return this.userService.isRequesting;
  }
  logout() {
      this.userService.logout();
  }
  logUsers() {
      this.dataService.getData('api/users', (users) => {
      }, (error) => {
      });
  }
}
